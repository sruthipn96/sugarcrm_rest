require "sugarcrm_rest/version"
require 'active_support'
require "net/https"
require "rest-client"
require "uri"
require "rubygems"
require "json"

module SugarcrmRest
  
 class Connect
	attr_reader :consumer_key,:consumer_secret,:username,:password,:platform,:url
 def initialize(consumer_key,consumer_secret,username,password,platform,url)
	@consumer_key=consumer_key
	@consumer_secret=consumer_secret
	@username=username
	@password=password
	@platform=platform
	@url=url
 end
end
  #class for authenticattion
  class Get_Token_Process_Url
    $count=0
	
    def self.authenticate(connect)
		begin
			url =''+"#{connect.url}"+'oauth2/token'
			oauth2_token_arguments = {"grant_type" =>"password", "client_id" =>connect.consumer_key, "client_secret" =>connect.consumer_secret, "username" =>connect.username, "password" =>connect.password, "platform" =>connect.platform}
			uri = URI.parse url
			http = Net::HTTP.new uri.host, uri.port
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			request = Net::HTTP::Post.new uri.request_uri
			request["Content-Type"] = "application/x-www-form-urlencoded"
			request.body = URI.encode_www_form(oauth2_token_arguments)  
			response = http.request request
			token = nil
			if response.kind_of? Net::HTTPSuccess
				token = JSON.parse response.body
			end
			
			return token['access_token']
		end
		rescue Exception=>e
			
			retry if ($count += 1) < 3
		
    end


    
    def self.execute_uri(connect,url)
	
		begin
			uri = URI.parse url
			http = Net::HTTP.new uri.host, uri.port
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			request = Net::HTTP::Get.new uri.request_uri
			request["Content-Type"] = "application/json"
			request["OAuth-Token"] = authenticate(connect)
			response = http.request request
		end
		rescue Exception=>e
		
			
			retry if ($count += 1) < 3
	end

  end




 
class Fetch_Data
   
	#Fetch a single record by the given id from given module
	def self.fetch_single_record(connect,module_name,id)  
		url = ''+"#{connect.url}"+"#{module_name}/#{id}"
		response = Get_Token_Process_Url.execute_uri(connect,url)
		if response.kind_of? Net::HTTPSuccess
			root = JSON.parse response.body
			return root
		end
	end
	#fetch all data from agiven module
	def self.fetch_all_data(connect,module_name,offset=0,max_num=100)
		url = ''+"#{connect.url}"+"#{module_name}"+"?offset="+"#{offset}"+"&max_num="+"+#{max_num}"+''
		response = Get_Token_Process_Url.execute_uri(connect,url)
		if response.kind_of? Net::HTTPSuccess
			root = JSON.parse response.body
			
			return root
		end
	end

	def self.adding_fields(connect,module_name,filter,fields)
		offset=0
		count=0
		if offset==0
			@array_all_claimants = Array.new
		end
		filter=filter+"&fields="
		fields.each do |f|
			filter=filter+f
			
			filter=filter+"," if (count += 1) < fields.length
		end
		
		result=Hash.new
		i=0
		while offset >= 0 do
			url =''+"#{connect.url}"+"#{module_name}"+"?filter=#{filter}"+'&max_num=100&offset='+offset.to_s
			
			response = Get_Token_Process_Url.execute_uri(connect,url)
			if response.kind_of? Net::HTTPSuccess
				root = JSON.parse response.body 
				offset=root['next_offset'] 
					 
				root['records'].each do |r|
					result[i]={fields[0]=>r[fields[0]]}
					for f in 1..fields.length-1
						result[i]=result[i].merge(fields[f]=>r[fields[f]])
					end
					i=i+1
				end 
			end
		end
		return result.to_json
	end
	
	def self.fetch_data_with_filters(connect,module_name,filter_json,offset=0,fields=["no"])
		filter=filter_json
		
		
		if (fields[0]=="no")
		
			url =''+"#{connect.url}"+"#{module_name}"+"?filter=#{filter_json}"+'&max_num=100&offset='+offset.to_s
			
			response = Get_Token_Process_Url.execute_uri(connect,url)
		
			root = JSON.parse response.body 
		
			if response.kind_of? Net::HTTPSuccess
				root = JSON.parse response.body 
				return root 
			end 
		else
			return adding_fields(connect,module_name,filter,fields)
		end
		
	end
	
	
	def self.upload_document(connect,id,path)

		@token=Get_Token_Process_Url.authenticate(connect)
		f=File.new(path,'rb')
		response =  RestClient::Request.execute(
  				:url => "#{connect.url}/Documents/#{id}/file/filename", 
  				:method => :post, 
 			 	:headers => {:'OAuth-Token' => @token},
 			 	:payload => {:filename => f},
  				:verify_ssl => false
				)
				
			 if(response.code==200)
				root = JSON.parse response.body 
				record=root['record']
			end
				return record['id']
			

	end
	


 end
 
 
 
 class Update_Sugar
  
	def self.create_new_record(connect,module_name,params)

		url=''+"#{connect.url}"+module_name
		uri = URI.parse url
		http = Net::HTTP.new(uri.host, uri.port)
		request = Net::HTTP::Post.new(uri)
		request["OAuth-Token"] = Get_Token_Process_Url.authenticate(connect)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request.add_field('Content-Type','application/json')
		request.body=params.to_json
		response = http.request request
		
		if response.kind_of? Net::HTTPSuccess
			root = JSON.parse response.body 
		end
		return root
	end
	
	
	def self.update_data(connect,module_name,params,id)
	
		url=''+"#{connect.url}"+module_name+'/'+id
		uri = URI.parse url
		http = Net::HTTP.new(uri.host, uri.port)
		request = Net::HTTP::Put.new(uri)
		request["OAuth-Token"] = Get_Token_Process_Url.authenticate(connect)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request.add_field('Content-Type','application/json')
		request.body=params.to_json
      
		response = http.request request
		
		if response.kind_of? Net::HTTPSuccess
			root = JSON.parse response.body 
		end
		return root
	end

	def self.delete_record(connect,module_name,id)
		url=''+"#{connect.url}"+module_name+'/'+id
		uri = URI.parse url
		http = Net::HTTP.new(uri.host, uri.port)
		request = Net::HTTP::Delete.new(uri)
		request["OAuth-Token"] = Get_Token_Process_Url.authenticate(connect)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE
		request.add_field('Content-Type','application/json')
      
		response = http.request request
		
		if response.kind_of? Net::HTTPSuccess
			root = JSON.parse response.body 
		end
		return root
		end
 end
end
