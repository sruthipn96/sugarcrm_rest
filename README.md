# SugarcrmRest



## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sugarcrm_rest'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sugarcrm_rest

## Usage
```r
connect=SugarcrmRest::Connect.new(consumer_key,consumer_secret,username,password,platform,url) #Provide the connection object
SugarcrmRest::Fetch_Data.fetch_single_record(connect,module_name,id) #Give the record with specified id from specified module
SugarcrmRest::Fetch_Data.fetch_all_data(connect,module_name,offset,maxnum) #Fetch data from specified module according to offset
SugarcrmRest::Fetch_Data.fetch_data_with_filters(connect,module_name,filter_json,offset=0,fields=["no"]) #Return a json object of data having specified fields by applying specified filters.The filter format is filter=[{"name":"example"},{"age":{"$gte":age}}] 
SugarcrmRest::Fetch_Data.upload_document(connect,id,path) #upload the file having specified path and id to doccument module

SugarcrmRest::Update_Sugar.create_new_record(connect,module_name,params) #Creating new record in specified module 
SugarcrmRest::Update_Sugar.update_data(connect,module_name,params,id) #update fields of specified id and module
SugarcrmRest::Update_Sugar.delete_record(connect,module_name,id) #Delete specified record





```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/sugarcrm_rest.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
